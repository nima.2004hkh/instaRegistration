var mysql = require('mysql');

const Sequelize = require('sequelize');
const dbconfig = new Sequelize('sharafiShop', 'root', 'manimani', {
    host: 'localhost',
    dialect: 'mysql',
    operatorsAliases: false,
    pool: {
        max: 5,
        min: 0,
        acquire: 30000,
        idle: 10000
    },
});


module.exports = dbconfig;